/* 
* @Author: Stefan Wirth
* @Date:   2015-09-29 17:40:42
* @Last Modified by:   Stefan Wirth
* @Last Modified time: 2015-12-14 23:49:36
*/
(function() {
    'use strict';

    /**
     * @ngdoc function
     * @name adminDashboardApp.controller:MainCtrl
     * @description
     * # MainCtrl
     * Controller of the adminDashboardApp
     */
    angular.module('niboDashboardApp')
        .controller('NiboOverviewCtrl', [
            '$sails',
            '$http',
            'nibos',
            NiboOverviewController
        ]);



    function NiboOverviewController($sails, $http, nibos) {
        var vm = this;
        vm.nibos = nibos;
        vm.simulateIncident = simulateIncident;

        $sails.on('nibo', function(message) {
            if(message.verb !== 'created' && message.verb !== 'updated') {
                return;
            }

            if(message.verb === 'created') {
                vm.nibos.push(message.data);
            } else if(message.verb === 'updated') {
                console.log(message);
                vm.nibos = vm.nibos.map(function(nibo) {
                    if(nibo.id === message.id) {
                        nibo.status = message.data.status;
                    }
                    return nibo;
                });
            }
        });

        function simulateIncident(niboId) {
            console.log(niboId);
            $sails.post('/incidents', {nibo: niboId})
                .then(function(createdIncident) {
                    console.log(createdIncident);
                })
                .catch(function(err) {
                    console.error(err);
                });
        }
    };
}());