/* 
* @Author: Stefan Wirth
* @Date:   2015-09-29 17:40:42
* @Last Modified by:   Stefan Wirth
* @Last Modified time: 2015-10-20 17:02:04
*/
(function() {
    'use strict';

    /**
     * @ngdoc function
     * @name adminDashboardApp.controller:MainCtrl
     * @description
     * # MainCtrl
     * Controller of the adminDashboardApp
     */
    angular.module('niboDashboardApp')
        .controller('IncidentsCtrl', [
            '$sails',
            'incidents',
            IncidentsController
        ]);

    function IncidentsController($sails, incidents) {
        var vm = this;
        vm.incidents = incidents;
        $sails.on('incident', function(message) {
            if(message.verb !== 'created' && message.verb !== 'updated') {
                return;
            }

            if(message.verb === 'created') {
                console.log('adding incident');
                vm.incidents.unshift(message.data);
            } else if(message.verb === 'updated') {
                vm.incidents = vm.incidents.map(function(incident) {
                    if(incident.id === message.id) {
                        incident.resolvedAt = message.data.resolvedAt;
                    }
                    return incident;
                });
            }
        });
    };
}());