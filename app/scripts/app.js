'use strict';

/**
 * @ngdoc overview
 * @name niboDashboardApp
 * @description
 * # niboDashboardApp
 *
 * Main module of the application.
 */
angular
  .module('niboDashboardApp', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngSanitize',
    'ngSails',
    'ui.router',
    'smart-table',
    'ngTouch'
  ])
  .config(['$urlRouterProvider', '$stateProvider', function ($urlRouterProvider, $stateProvider) {
    $urlRouterProvider.otherwise('/');

    $stateProvider
      .state('index', {
        url: '/',
        templateUrl: '/views/main.html',
        controller: 'NiboOverviewCtrl as vm',
        resolve: {
          nibos: ['$resource', function($resource) {
            return $resource('http://localhost:1234/nibos/:id', {id: '@id'}).query().$promise;
          }]
        }
      })
      .state('showNiboIncidents', {
        url: '/nibos/:niboId/incidents',
        templateUrl: '/views/about.html',
        controller: 'IncidentsCtrl as vm',
        resolve: {
          incidents: ['$resource', '$stateParams', function($resource, $stateParams) {
            var niboId = parseInt($stateParams.niboId);
            return $resource('http://localhost:1234/nibos/:id/incidents', {id: '@id'})
              .query({id: niboId, sort: 'createdAt DESC'})
              .$promise;
          }]
        }
      })
  }])
  .config(['$sailsProvider', function($sailsProvider) {
    //configure socket stuff
    $sailsProvider.url = 'http://localhost:1234';
}]);
