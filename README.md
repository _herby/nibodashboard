# nibo-dashboard

This project is generated with [yo angular generator](https://github.com/yeoman/generator-angular)
version 0.12.1.

## Build & development
install node:
https://nodejs.org/en/

install grunt:
http://gruntjs.com/

install bower
http://bower.io/

open console/terminal:

`$ npm install (installs node deps)`

`$ bower install (installs frontend deps)`

Run `$ grunt serve` for preview.